#!/bin/sh
set -e

# This will only remove masks created by d-s-h on package removal.
deb-systemd-helper unmask ceph_exporter.service >/dev/null || true

# was-enabled defaults to true, so new installations run enable.
if deb-systemd-helper --quiet was-enabled ceph_exporter.service; then
	# Enables the unit on first installation, creates new
	# symlinks on upgrades if the unit file has changed.
	deb-systemd-helper enable ceph_exporter.service >/dev/null || true
else
	# Update the statefile to add new symlinks (if any), which need to be
	# cleaned up on purge. Also remove old symlinks.
	deb-systemd-helper update-state ceph_exporter.service >/dev/null || true
fi

if [ -x "/etc/init.d/ceph_exporter" ]; then
	update-rc.d ceph_exporter defaults >/dev/null
	invoke-rc.d ceph_exporter start || exit $?
fi

if [ -d /run/systemd/system ]; then
	systemctl --system daemon-reload >/dev/null || true
	deb-systemd-invoke start ceph_exporter.service >/dev/null || true
fi
