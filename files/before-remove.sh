#!/bin/sh
set -e

if [ -d /run/systemd/system ]; then
	deb-systemd-invoke stop ceph_exporter.service >/dev/null
fi

if [ -x "/etc/init.d/ceph_exporter" ]; then
	invoke-rc.d ceph_exporter stop || exit $?
fi
