#!/usr/bin/env bash
set -xe

OS_CODENAME=${1:-all}
#PATH=${GOROOT}/bin:${PATH}

mkdir -p ${GOPATH}
mkdir -p ${GOROOT}

curl -s 'https://download.ceph.com/keys/release.asc' | apt-key add -
echo "deb https://download.ceph.com/debian-luminous xenial main" >> /etc/apt/sources.list

apt-get update
apt-get -y  install librados-dev librbd-dev

git clone --branch ${BRANCH} https://${PACKAGE_SOURCE_URL}.git ${GOPATH}/src/${PACKAGE_SOURCE_URL}
curl https://storage.googleapis.com/golang/go1.9.2.linux-amd64.tar.gz | tar xzf - -C ${GOROOT}  --strip-components=1

cd ${GOPATH}/src/${PACKAGE_SOURCE_URL}
go get -d
go build -o ${GOPATH}/${CI_PROJECT_NAME}

mkdir -p /builds/packages/${CI_PROJECT_NAME}/deb

gem install fpm

/usr/local/bin/fpm \
  -f -m "${MAINTAINER}" \
  -t deb -s dir -n ${CI_PROJECT_NAME} \
  -p /builds/packages/${CI_PROJECT_NAME}/deb \
  -v "$(date +%s)" \
  --description "$(cat /builds/packages/${CI_PROJECT_NAME}/files/description)" \
  --iteration ${CI_PIPELINE_ID}+${OS_CODENAME} \
  --after-install /builds/packages/${CI_PROJECT_NAME}/files/after-install.sh \
  --after-remove /builds/packages/${CI_PROJECT_NAME}/files/after-remove.sh \
  --before-remove /builds/packages/${CI_PROJECT_NAME}/files/before-remove.sh \
  --config-files /etc/default/${CI_PROJECT_NAME} \
  --deb-systemd=/builds/packages/${CI_PROJECT_NAME}/files/${CI_PROJECT_NAME}.service \
  /builds/packages/${CI_PROJECT_NAME}/files/${CI_PROJECT_NAME}.default=/etc/default/${CI_PROJECT_NAME} \
  ${GOPATH}/${CI_PROJECT_NAME}=/usr/bin/${CI_PROJECT_NAME}
